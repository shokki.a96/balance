import { useState } from 'react';
import Button1 from '../buttons/button1';
import Search from './Search';
import NavbarMenu from '../header/NavbarMenu';
import LangDropdown from '../header/LangDropdown';

const Header = () => {

    return (
        <header className='header'>
            <nav className="navbar navbar-expand-lg shadow-sm">
                <div className="container">
                    <a className="navbar-brand" href="/" style={{ marginRight: '30px' }}>
                        <img src='assets/icons/logo.svg' />
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse gap-5" id="navbarSupportedContent">
                        <Button1 />
                        <Search />
                        <NavbarMenu />
                        <LangDropdown />
                    </div>
                </div>
            </nav>
        </header>
    )
}

export default Header