import React from 'react'

const NavbarMenu = () => {
    return (
        <div className='navbar-menu'>
            <div className='navbar-menu-item'>
                <div className='navbar-menu-item-img'><img src="assets/icons/fav.svg" alt="" /></div>
                <span>Избранное</span>
            </div>
            <div className='navbar-menu-item'>
                <div className='navbar-menu-item-img'><img src="assets/icons/chat.svg" alt="" /></div>
                <span>Чат</span>
            </div>
            <div className='navbar-menu-item navbar-menu-item--active'>
                <div className='navbar-menu-item-img'><img src="assets/icons/user.svg" alt="" /></div>
                <span>Анастасия</span>
            </div>
        </div>
    )
}

export default NavbarMenu