import React, { useState } from 'react';
const Card = () => {

    const [usdBalance, setUsdBalance] = useState()
    const [rublBalance, setRublBalance] = useState()

    const handleUsdChange = (e) => {
        const usd = e;
        setUsdBalance(usd);
        setRublBalance(usd * 15);
    };

    const handleRublChange = (e) => {
        const rubl = e;
        setRublBalance(rubl);
        setUsdBalance(rubl / 15);
    };


    const [cardNumber, setCardNumber] = useState('');
    const [isValid, setIsValid] = useState(true);

    const handleCardNumberChange = (e) => {
        let inputCardNumber = e.target.value;
        // Remove any non-digit characters from the input
        inputCardNumber = inputCardNumber.replace(/\D/g, '');
        // Limit the input to 16 digits
        inputCardNumber = inputCardNumber.slice(0, 16);
        // Check if the card number has 16 digits and only contains digits
        const isValidCardNumber = /^\d{16}$/.test(inputCardNumber);
        setIsValid(isValidCardNumber);
        // Separate every 4 digits with a space for masking
        const maskedCardNumber = inputCardNumber.replace(/(.{4})/g, '$1 ');
        setCardNumber(maskedCardNumber);
    };


    const [month, setMonth] = useState('');
    const [year, setYear] = useState('');
    const [isValidMonth, setIsValidMonth] = useState(true);
    const [isValidYear, setIsValidYear] = useState(true);

    const handleMonthChange = (e) => {
        let inputMonth = e.target.value;
        // Remove any non-digit characters from the input
        inputMonth = inputMonth.replace(/\D/g, '');
        // Limit the input to 2 digits
        inputMonth = inputMonth.slice(0, 2);
        // Check if the month has 2 digits and is between 01 and 12
        const isValidMonth = /^(0[1-9]|1[0-2])$/.test(inputMonth);
        setIsValidMonth(isValidMonth);
        setMonth(inputMonth);
    };

    const handleYearChange = (e) => {
        let inputYear = e.target.value;
        // Remove any non-digit characters from the input
        inputYear = inputYear.replace(/\D/g, '');
        // Limit the input to 4 digits
        inputYear = inputYear.slice(0, 4);
        // Check if the year has 4 digits
        const isValidYear = /^\d{4}$/.test(inputYear);
        setIsValidYear(isValidYear);
        setYear(inputYear);
    };


    const [cvv, setCVV] = useState('');
    const [isValidCVV, setIsValidCVV] = useState(true);

    const handleCVVChange = (e) => {
        let inputCVV = e.target.value;
        // Remove any non-digit characters from the input
        inputCVV = inputCVV.replace(/\D/g, '');
        // Limit the input to 3 digits
        inputCVV = inputCVV.slice(0, 3);
        // Check if the CVV has 3 digits
        const isValidCVV = /^\d{3}$/.test(inputCVV);
        setIsValidCVV(isValidCVV);
        setCVV(inputCVV);
    };


    const [isChecked, setIsChecked] = useState(false);
    const [isCheckedError, setIsCheckedError] = useState(false);

    const handleCheckboxChange = () => {
        setIsChecked(!isChecked);
        setIsCheckedError(false); // Reset the error when the checkbox is toggled
    };

    const handlePayment = () => {
        if (!isChecked) {
            setIsCheckedError(true); // Set the error state if the checkbox is not checked
            return; // Stop further processing
        }
        // Proceed with payment action if checkbox is checked
        console.log('Payment successful!');
    };



    return (
        <div className='card card-wrapper shadow-sm'>
            <div className='card-body card-body-wrapper'>
                <h1 className='card-title'>Пополнить банковской картой</h1>

                <div className='card-convertation'>
                    <h3>Укажите сумму</h3>
                    <div className='card-convertation-body'>
                        <div className='card-convertation-item'>
                            <input type="text" placeholder='0000.00' value={usdBalance} onChange={(e) => handleUsdChange(e.target.value)} />
                            <img src="/assets/icons/ballance.svg" alt="balance icon" />
                        </div>
                        <div className='card-convertation-item'>
                            <input type="text" placeholder='0000.00' value={rublBalance} onChange={(e) => handleRublChange(e.target.value)} />
                            <img src="/assets/icons/rub.svg" alt="balance icon" />
                        </div>
                    </div>
                </div>

                <div className='card-selected'>
                    <div className='curent-card'>
                        <div>•••• 3282</div>
                        <div>12  /  23</div>
                    </div>
                    <div className='add-card'>
                        <div className='add-card-body'>
                            <img src="/assets/icons/add.svg" alt="plus icon" />
                            <span>Новая карта</span>
                        </div>
                    </div>
                </div>

                <div className='row'>
                    <div className='cards-body-wrapper'>
                        <img src="/assets/bg/card_bg1.svg" alt="card bg primary" className='card-size-primary' />
                        <img src="/assets/bg/card_bg2.svg" alt="card bg secondary" className='card-size-secondary' />

                        <div className='card-inputs'>
                            <div className='input-primary'>
                                <h3>Номер карты</h3>
                                <input
                                    type="text"
                                    placeholder='Номер карты'
                                    value={cardNumber}
                                    onChange={handleCardNumberChange}
                                />

                                <h3>Действует до</h3>
                                <div className='card-date'>
                                    <input type="number" placeholder='мм' value={month}
                                        onChange={handleMonthChange} />
                                    <span>/</span>
                                    <input type="number" placeholder='гг' value={year}
                                        onChange={handleYearChange} />
                                </div>

                            </div>
                            <div className='input-secondary'>
                                <h3>CVV/CVC</h3>
                                <input type="number" placeholder='000'
                                value={cvv}
                                onChange={handleCVVChange} />
                                <h6 className='cvc-text'>
                                    три цифры с обратной стороны карты
                                </h6>
                            </div>
                        </div>
                    </div>


                    <div className='errors'>
                        {!isValid && <p style={{ color: 'red' }}>Номер карты должен содержать 12 цифр</p>}

                        {!isValidMonth && <p style={{ color: 'red' }}>Месяц должен быть в формате MM и в диапазоне от 01 до 12</p>}
                        {!isValidYear && <p style={{ color: 'red' }}>Год должен быть в формате YYYY</p>}

                        {!isValidCVV && <p style={{ color: 'red' }}>CVV/CVC должен содержать 3 цифры</p>}
                       
                        {isCheckedError && <p style={{ color: 'red' }}>Пожалуйста, согласитесь с условиями привязки карты.</p>}
                    </div>

                    <div className='col-md-12'>
                        <div className="acception-wrapper">
                            <div className='acception'>
                                <label class="checkbox path">
                                    <input type="checkbox"  checked={isChecked} onChange={handleCheckboxChange} />
                                    <svg viewBox="0 0 21 21">
                                        <path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
                                    </svg>
                                </label>

                                <h6>
                                    Запомнить эту карту. Это безопасно.
                                </h6>
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.833332 10C0.833332 4.93743 4.93739 0.833374 10 0.833374C15.0626 0.833374 19.1667 4.93743 19.1667 10C19.1667 15.0626 15.0626 19.1667 10 19.1667C4.93739 19.1667 0.833332 15.0626 0.833332 10ZM10 2.50004C5.85786 2.50004 2.5 5.8579 2.5 10C2.5 14.1422 5.85786 17.5 10 17.5C14.1421 17.5 17.5 14.1422 17.5 10C17.5 5.8579 14.1421 2.50004 10 2.50004Z" fill="#C7C9D9" />
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10 5.83337C10.4602 5.83337 10.8333 6.20647 10.8333 6.66671V10C10.8333 10.4603 10.4602 10.8334 10 10.8334C9.53976 10.8334 9.16666 10.4603 9.16666 10V6.66671C9.16666 6.20647 9.53976 5.83337 10 5.83337Z" fill="#C7C9D9" />
                                    <path d="M10.8333 13.3334C10.8333 13.7936 10.4602 14.1667 10 14.1667C9.53976 14.1667 9.16666 13.7936 9.16666 13.3334C9.16666 12.8731 9.53976 12.5 10 12.5C10.4602 12.5 10.8333 12.8731 10.8333 13.3334Z" fill="#C7C9D9" />
                                </svg>
                            </div>
                            <div className='acception2'>
                                <h6>
                                    Сохраняя карту, вы соглашаетесь с <a href='https://google.com' target='_blank'>условиями привязки карты.</a>
                                </h6>
                            </div>
                            <div className='btn-section'>
                                <button class="btn btn-primary" type="button" onClick={handlePayment}>Оплатить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card