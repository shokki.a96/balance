import * as React from "react";

function Button1() {
    return (
        <>
            <style jsx>{`
                .btn {
                background-color: var(--Primary-primary-01---main, #3e7bfa);
                display: flex;
                gap: 8px;
                }
            `}</style>

            <button type="button" className="btn btn-primary" style={{ background: '#3E7BFA', paddingLeft: '15px', paddingRight: '15px', paddingTop: '10px', paddingBottom: '10px' }}>
                <img src='assets/icons/vitrina.svg' /> Витрина
            </button>
        </>
    );
}

export default
    Button1;