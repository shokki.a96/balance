const Tab = () => {

    return (
        <div className='bg-bread'>
        <div className='container'>
          <div className='tab-wrapper'>
            <a href='#' className='tab-item active-tab'>
              Баланс
            </a>
            <a href='#' className='tab-item'>
              Олимп
            </a>
            <a href='#' className='tab-item'>
              Избранное
            </a>
            <a href='#' className='tab-item'>
              Чат
            </a>
            <a href='#' className='tab-item'>
              Профиль
            </a>
            <a href='#' className='tab-item'>
              Партнёрам
            </a>
            <a href='#' className='tab-item'>
              Партнёрам
            </a>
          </div>
          <div className='breadcrumb-wrapper'>
            <div className='bread-item'>
              Баланс
            </div>
            <div className='bread-dot'> 
            •
            </div>
            <div className='bread-item active-bread'>
            Пополнить основной счёт
            </div>
          </div>
        </div>
      </div>
    )
}

export default Tab