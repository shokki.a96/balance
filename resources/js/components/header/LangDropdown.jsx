import React, { useState } from 'react'

const LangDropdown = () => {
    const [langOpened, setLangOpened] = useState(false)


    return (
        <div className='search-dropdown-r' onClick={() => setLangOpened(prev => !prev)}>
            <span>RU</span>
            <div className='search-dropdown-icon'>
                <img src="/assets/icons/arrow-down.svg" alt="book-icon" />
            </div>
            {
                langOpened && <div className='search-dropdown-opened'>
                    <span>RU</span>
                    <span>En</span>
                </div>
            }
        </div>
    )
}

export default LangDropdown