import React, { useState } from 'react'

const Search = () => {
    const [dropdownOpened, setDropdownOpened] = useState(false)

    return (
        <div className='search-section'>
            <div className='search-dropdown' onClick={() => setDropdownOpened(prev => !prev)}>
                <div className='search-dropdown-icon'>
                    <img src="/assets/icons/book.svg" alt="book-icon" />
                </div>

                <span>Книги</span>

                <div className='search-dropdown-icon'>
                    <img src="/assets/icons/arrow-down.svg" alt="book-icon" />
                </div>

                {
                    dropdownOpened && <div className='search-dropdown-opened'>
                        <span>Clothes</span>
                        <span>Pc</span>
                        <span>Books</span>
                    </div>
                }
            </div>

            <div className='search'>
                <input type="text" placeholder='Поиск книги' />
                <button><img src="assets/icons/search.svg" alt="" /></button>
            </div>
        </div>)
}

export default Search