import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import Button1 from './components/buttons/button1';
import Header from './components/global/Header';
import Tab from './components/global/Tab';
import "../css/app.css";
import Footer from './components/global/Footer';
import Card from './components/global/Card';




function Main() {

  return (
    <div className="body-balance">
      <Header />

      <Tab />

      <Card />

      <Footer />
    </div>
  );
}

export default Main;

if (document.getElementById('main')) {
  const Index = ReactDOM.createRoot(document.getElementById("main"));

  Index.render(
    <React.StrictMode>
      <Main />
    </React.StrictMode>
  )
}
