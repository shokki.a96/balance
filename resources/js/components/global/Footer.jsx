const Footer = () => {

    return (
        <footer>
            <div className='container'>
                <div className='align-items-center footer-contents justify-content-start'>
                    <h6>
                        ©X11 2021
                    </h6>

                    <a href='https://google.com' target='_blank'>
                        Правовая информация
                    </a>
                </div>
            </div>
        </footer>
    )
}

export default Footer